# ----------------------------------- About Template -----------------------------------

# Author: Sine

# Prerequisites:

Framework: Express
Language: TypeScript
ORM: Prisma
Webpack:

# Install dependencies:

npm install

# Run project

npm start

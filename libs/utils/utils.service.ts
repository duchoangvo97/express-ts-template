export class UtilsService {
    private static _instance: UtilsService;

    static getInstance() {
        if (this._instance) return this._instance;

        this._instance = new UtilsService();
        Object.freeze(this._instance);
        return this._instance;
    }
}

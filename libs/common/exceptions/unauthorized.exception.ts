import { BaseException } from "./base-exeption";

export class UnauthorizedException extends BaseException {
    public constructor(message: string);
    public constructor(objectOrError: any);
    public constructor(message?: string, objectOrError?: any);

    constructor(...args: any[]) {
        args.length === 2 ? super(401, args[0], args[1]) : super(401, args[0]);
    }
}

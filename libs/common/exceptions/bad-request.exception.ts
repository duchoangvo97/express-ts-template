import { BaseException } from "./base-exeption";

export class BadRequestException extends BaseException {
    public constructor(message: string);
    public constructor(objectOrError: any);
    public constructor(message?: string, objectOrError?: any);

    constructor(...args: any[]) {
        args.length === 2 ? super(400, args[0], args[1]) : super(400, args[0]);
    }
}

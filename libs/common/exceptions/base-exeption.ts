export class BaseException extends Error {
    statuscode: number;
    objectOrError?: any;

    public constructor(statuscode: number, message?: string);
    public constructor(statuscode: number, objectOrError: any);
    public constructor(
        statuscode: number,
        message?: string,
        objectOrError?: any
    );

    public constructor(...args: any[]) {
        super(typeof args[1] === "string" ? args[1] : undefined);

        this.statuscode = args[0];
        this.objectOrError = typeof args[1] !== "string" ? args[1] : undefined;

        this.name = this.constructor.name;

        Error.captureStackTrace(this, this.constructor);
    }
}

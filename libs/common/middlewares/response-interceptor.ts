import { NextFunction, Request, Response } from "express";

export class ResponseInterceptor {
    static GlobalResponseInterceptor(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        const status = res.statusCode;
        const resData = res.json;
        res.status(status).json = (data) => {
            res.json = resData;
            return res.json(data);
        };
        next();
    }
}

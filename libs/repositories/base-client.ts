import { PrismaClient } from "@prisma/client";
import { Sql } from "@prisma/client/runtime";

export class BaseClient {
    protected readonly _client: PrismaClient;
    constructor() {
        this._client = new PrismaClient({
            log: ["query", "info", `warn`, `error`],
        });
    }

    public queryRaw<T = any>(
        query: string | TemplateStringsArray | Sql,
        ...values: any[]
    ): Promise<T> {
        this._client.$queryRaw;
        return this._client.$queryRaw<T>`${query}, ${values}`;
    }
}

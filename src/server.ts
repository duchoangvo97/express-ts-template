import express, { NextFunction, Request, Response } from "express";
import cors from "cors";
import helmet from "helmet";
import { WebpackHotModule } from "../libs/common/interfaces/webpack-hotmodule.interface";
import { ConfigsService } from "../libs/configs/config.service";
import { GlobalResponseInterceptor } from "../libs/common/middlewares";

const PORT = ConfigsService.getInstance().getNumber("PORT") || 80;

const app = express();

app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(GlobalResponseInterceptor);

app.get("/", (req, res) => {
    //throw new BadRequestException("hihi");
    // throw new BadRequestException([{ field: "us", mess: "aaa" }]);

    // res.send("hih");

    res.status(200).json({ message: "hihi" });
});

app.use(async (err: Error, req: Request, res: Response, next: NextFunction) => {
    console.log("error:", err);
});

const server = app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});

declare const module: WebpackHotModule;

if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => server.close());
}

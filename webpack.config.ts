import { ConfigsService } from "./libs/configs/config.service";
import webpack from "webpack";
import path from "path";
import nodeExternals from "webpack-node-externals";
import nodemonPlugin from "nodemon-webpack-plugin";

const plugins = ConfigsService.getInstance().isDevelopment
    ? [
          new webpack.HotModuleReplacementPlugin(),
          new nodemonPlugin({ script: "./dist/server.js" }),
      ]
    : [new webpack.HotModuleReplacementPlugin()];

module.exports = {
    entry: ["webpack/hot/poll?100", "./src/server.ts"],
    watch: true,
    target: "node",
    externals: [nodeExternals({ allowlist: ["webpack/hot/poll?100"] })],
    module: {
        rules: [{ test: /.tsx?$/, use: "ts-loader", exclude: /node_modules/ }],
    },
    mode: "development",
    resolve: { extensions: [".tsx", ".ts", ".js"] },
    plugins,
    output: { path: path.join(__dirname, "dist"), filename: "server.js" },
};
